Para pasar a producción:

1) Se debe crear la base de datos en el server junto con las tablas "avisos" y "errores" con los
siguientes atributos:

Tabla avisos

+----------+--------------+------+-----+---------+----------------+
| Field    | Type         | Null | Key | Default | Extra          |
+----------+--------------+------+-----+---------+----------------+
| link     | varchar(200) | YES  |     | NULL    |                |
| fecha    | date         | YES  |     | NULL    |                |
| producto | varchar(20)  | YES  |     | NULL    |                |
| id       | int(11)      | NO   | PRI | NULL    | auto_increment |
+----------+--------------+------+-----+---------+----------------+

Tabla errores:

+----------+-------------+------+-----+---------+----------------+
| Field    | Type        | Null | Key | Default | Extra          |
+----------+-------------+------+-----+---------+----------------+
| fecha    | date        | YES  |     | NULL    |                |
| producto | varchar(20) | YES  |     | NULL    |                |
| id       | int(11)     | NO   | PRI | NULL    | auto_increment |
+----------+-------------+------+-----+---------+----------------+

2) Se debe crear un entorno virtual e instalar las dependencias que se encuentran en requeriments.txt

3) Luego de haber creado la base y las tablas se debe correr el script migracion.py que pasa los 
datos de los archivos a la base. Para que funcione, se debe correr desde la carpeta Avisos (por el path utilizado).

4) Crontabear el script run.sh que levanta el entorno virtual y corre el script main.py.

Para escalarlo:

- Si se quiere agregar un nuevo producto:

    1) Se debe agregar una subclase que herede de Producto. Esta subclase sólo definirá el método que formatea 
    el html de la página del producto y retorna un arreglo de links.

    2) Se debe agregar en el archivo config.py. El nombre del producto del archivo config debe ser igual al 
    nombre de la clase.

    3) Agregar el producto en el archivo __init__.py para que main.py funcione correctamente.

    4) Actualizar la base de datos: se puede hacer un script que migre todos los links existentes en la página
    de avisos del producto para evitar que se alerten todos los avisos de la página. 

- Si se quiere modificar la manera en que se manejan los errores, puede utilizarse un Strategy o un Decorator.


