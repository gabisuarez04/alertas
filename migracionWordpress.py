from bs4 import BeautifulSoup
import requests

from DB.Base import *

urls = []

html = requests.get("https://wordpress.org/news/category/security/").text
soup = BeautifulSoup(html, 'html.parser')	
tabla = soup.find(class_="widefat")
listado = tabla.find_all("tr")

base = Base()
           
for item in listado:
    link = item.find("a")
    url = link["href"]
    urls.append(url)

for url in urls:
    base.insertar_aviso("Wordpress", url)

