import json
import os

from DB.Base import *

def migrar_avisos(producto, path):
    links = json.loads(open(path, "r").read())
    if not "VMware" in producto:
        for url in links:
            base.insertar_aviso(producto, url)
    else:
        for url in links:
            link = "https://www.vmware.com"+url[23:]
            base.insertar_aviso(producto, link)

base = Base()
base.limpiar_tablas()

path_abs = os.path.abspath(__file__)[:-19]

path_adobe = path_abs+"adobe/links"
path_apache = path_abs+"apache/links"
path_drupal = path_abs+"drupal/links"
path_firefox = path_abs+"firefox/links"
path_joomla = path_abs+"joomla/links"
path_vmware = path_abs+"vmware/links"

migrar_avisos("Adobe", path_adobe)
migrar_avisos("Apache", path_apache)
migrar_avisos("Drupal", path_drupal)
migrar_avisos("Firefox", path_firefox)
migrar_avisos("Joomla", path_joomla)
migrar_avisos("VMware", path_vmware)

# Para VMware hay que sacar la doble // en "security" porque cada link se almacenó mal


