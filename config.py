# Los productos deben llamarse exactamente igual al nombre de sus clases para que el main.py funcione

mysql = dict(
    host = 'localhost',
    user = 'seguridad',
    passwd = 'qeadzcwsx',
    database = 'alertas'
)

productos = dict(
    Adobe = dict(
        sitio = 'https://helpx.adobe.com',
        pagina_avisos = 'https://helpx.adobe.com/la/security.html' 
    ),
    Firefox = dict(
        sitio = 'https://www.mozilla.org',
        pagina_avisos = 'https://www.mozilla.org/en-US/security/known-vulnerabilities/firefox/'
    ),
    VMware = dict(
        sitio = 'https://www.vmware.com',
        pagina_avisos = 'https://www.vmware.com/ar/security/advisories.html' 
    ),
    Drupal = dict(
        sitio = 'https://www.drupal.org',
        pagina_avisos = 'https://www.drupal.org/security/rss.xml' 
    ),
    Joomla = dict(
        sitio = 'https://developer.joomla.org',
        pagina_avisos = 'https://developer.joomla.org/security-centre.feed?type=rss' 
    ),
    Apache = dict(
        sitio = 'https://httpd.apache.org',
        pagina_avisos = 'https://httpd.apache.org/security/vulnerabilities_24.html' 
    ),
    Wordpress = dict(
        sitio = 'https://wordpress.org',
        pagina_avisos = 'https://wordpress.org/news/category/security/' 
    )
)
