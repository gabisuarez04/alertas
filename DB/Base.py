import mysql.connector
import datetime

import config as cfg

class Base(object):

    instancia = None

    def __new__(cls):
        if cls.instancia is None:
            cls.instancia = object.__new__(cls)
        return cls.instancia

    def __init__(self):
        try:
            self.mydb = mysql.connector.connect(
                host = cfg.mysql['host'],
                user = cfg.mysql['user'],
                passwd = cfg.mysql['passwd'],
                database = cfg.mysql['database']
            )
            
        except Exception as exc:
            print("ERROR al ejecutar el init de la clase Base: "+exc)
   
    def buscar_aviso(self, producto, link):
        mycursor = self.mydb.cursor()
        sql = "SELECT * FROM avisos WHERE producto = %s AND link = %s"
        val = (producto, link)
        mycursor.execute(sql, val)
        myresult = mycursor.fetchall()
        return myresult

    def insertar_aviso(self, producto, link):
        hoy = datetime.date.today()
        sql = "INSERT INTO avisos (link, fecha, producto) VALUES (%s, %s, %s)"
        val = (link, hoy, producto)
        mycursor = self.mydb.cursor()
        mycursor.execute(sql, val)
        self.mydb.commit()
        print("Se inserto el link "+link+" del producto "+producto)

    def buscar_error(self, producto, fecha):
        mycursor = self.mydb.cursor()
        sql = "SELECT * FROM errores WHERE producto = %s AND fecha = %s"
        val = (producto, fecha)
        mycursor.execute(sql, val)
        myresult = mycursor.fetchall()
        return myresult

    def insertar_error(self, producto):
        hoy = datetime.date.today()
        sql = "INSERT INTO errores (fecha, producto) VALUES (%s, %s)"
        val = (hoy, producto)
        mycursor = self.mydb.cursor()
        mycursor.execute(sql, val)
        self.mydb.commit()
        print("Se almaceno un error del producto "+producto)

    # Método definidos para migrar los datos de los archivos a la base de datos

    def limpiar_tablas(self):
        mycursor = self.mydb.cursor()
        mycursor.execute("DELETE FROM avisos")
        self.mydb.commit()
        mycursor.execute("DELETE FROM errores")
        self.mydb.commit()
        mycursor.execute("alter table avisos AUTO_INCREMENT = 1")
        self.mydb.commit()
        mycursor.execute("alter table errores AUTO_INCREMENT = 1")
        self.mydb.commit()
        print("Se limpiaron las tablas de la base")
