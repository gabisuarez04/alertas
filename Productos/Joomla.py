import feedparser

from .Producto import *

class Joomla(Producto):

    def obtener_links(self, html):
        a = feedparser.parse(self.pagina_avisos)
        urls=[]

        for elem in a.entries:
            urls.append(elem.link)
 
        return urls
