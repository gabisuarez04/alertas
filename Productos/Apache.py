from bs4 import BeautifulSoup

from .Producto import *

class Apache(Producto):

    def obtener_links(self, html):
    # Retorna un arreglo con los titulos de los fixes
        soup = BeautifulSoup(html, 'html.parser')
        soup = soup.find(id="apcontents")

        titles=[]
        titulos= soup.find_all("h1")[1:]

        for titulo in titulos:
            titles.append(titulo.text.strip())

        return titles