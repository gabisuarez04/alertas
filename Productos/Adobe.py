from .Producto import *

class Adobe(Producto):

    def obtener_links(self, html):
    # Retorna un arreglo con las urls
        urls = []
        tree = lxml.html.fromstring(html)
        tabla = tree.xpath("//table")
        tabla = tabla[1][0][1:]
        for i in range(len(tabla)):
            ok = False
            for j in tabla[i][0]:
                if not ok:
                    try:
                        url=j.attrib["href"]
                        ok=True
                    except:
                        None
            if url[:8]!="https://":
                url=self.sitio+url
            urls.append(url)
        return urls

    def print_data(self):
        print(self.nombre, self.sitio, self.pagina_avisos)
