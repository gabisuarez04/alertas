import lxml.html
import requests

from DB.Base import *

class Producto:
# Se podría usar un singleton para la base y pasarlo como parámetro en el init
    def __init__(self, nombre, sitio, pagina_avisos, error_handler):
        self.nombre = nombre
        self.sitio = sitio
        self.pagina_avisos = pagina_avisos
        self.db = Base()
        self.error_handler = error_handler

    def actualizar_avisos(self):
    # Las clases hijas implementan el método obtener_links
        try:
            html = requests.get(self.pagina_avisos).text
            links = self.obtener_links(html)
            self.actualizar_bd(links)
        except Exception as error:
            self.error_handler.manejar_error(self.nombre,error)

    def actualizar_bd(self, links):
    # Si la URL no está en la base, la manda al sistema de alertas y la guarda en la base
        for link in links:
            resultado = self.db.buscar_aviso(self.nombre, link)
            if not resultado:
                if (self.mandar_aviso_sistema(self.nombre, link)):
                    self.db.insertar_aviso(self.nombre, link)

    def mandar_aviso_sistema(self, producto, link):
    # Manda el aviso al sistema de alertas
        return True
        """a= requests.get("https://si.dpsit.gba.gob.ar/alertasmail/newlink?producto={}&link={}".format(producto,link),verify=False)
        print(a.text)
        if (a.text == "OK"):
        	return True
        else:
        	return False"""
