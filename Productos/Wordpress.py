from bs4 import BeautifulSoup

from .Producto import *

class Wordpress(Producto):
    
    def obtener_links(self, html):
    # Retorna un arreglo con las urls
        urls = []
        soup = BeautifulSoup(html, 'html.parser')	
        tabla = soup.find(class_="widefat")
        listado = tabla.find_all("tr")
               
        for item in listado:
            link = item.find("a")
            url = link["href"]
            urls.append(url)

        return urls

    
