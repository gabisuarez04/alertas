from bs4 import BeautifulSoup

from .Producto import *

class VMware(Producto):
    
    def obtener_links(self, html):
    # Retorna un arreglo con las urls
        urls = []
        soup = BeautifulSoup(html, 'html.parser')	
        listado = soup.find_all(class_="securityadvisorieslisting")
		        
        for item in listado:
            links = item.find_all("a")
            for elem in links:
                url = elem["href"]
                if url[:8]!="https://":
                    url = self.sitio + url 
                urls.append(url)
        
        return urls

    
