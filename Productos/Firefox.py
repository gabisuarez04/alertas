from bs4 import BeautifulSoup

from .Producto import *

class Firefox(Producto):
    
    def obtener_links(self, html):
    # Retorna un arreglo con las urls
        urls = []
        soup = BeautifulSoup(html, 'html.parser')	
        listado = soup.find_all(class_="level-item")
		
        for item in listado:
            url= item.find("a")
            if url!=None:
                urls.append(self.sitio+url["href"])
         
        return urls

   



