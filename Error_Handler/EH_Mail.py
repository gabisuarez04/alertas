# Esta clase es un tipo de manejador de errores que manda mail alertando sobre el mismo
# Si en un futuro se quiere agregar funcionalidad se puede hacer con un Decorator 
# Si se quieren utilizar distintos tipos de manejadores por producto se puede utilizar un Strategy

import smtplib
from email.mime.multipart import MIMEMultipart
from email.header import Header
from email.mime.text import MIMEText

from .Error_Handler import *

class EH_Mail(Error_Handler):
    
    def alertar_error(self, producto, error): # Puede recibir html, reciever, sender
        emisor = "reportesexternos@ciberseguridad.gba.gob.ar"
        receptor = "gabriela.suarez@gba.gob.ar"
        # Create message container - the correct MIME type is multipart/alternative.
        msg = MIMEMultipart('alternative')
        msg['Subject'] = Header("Falló el script del sitio de alertas del producto {}".format(producto).encode("utf-8"), "utf-8")
        msg['From'] = emisor
        msg['To'] = receptor
        SERVER = "mail.ciberseguridad.gba.gob.ar"
        FROM = "reportesexternos@ciberseguridad.gba.gob.ar"
        TO = ["gabriela.suarez@gba.gob.ar"] # tiene que ser una lista
        TEXT= "Fallo el script del producto {}. El error fue: <br><br> {} <br><br>".format(producto,error)
        part1 = MIMEText(TEXT, 'html')
        msg.attach(part1)
        server = smtplib.SMTP(SERVER)
        server.login("alertas@ciberseguridad.gba.gob.ar","Fb58LqqHm8zxS4De")
        server.sendmail(FROM, TO, msg.as_string())
        server.quit()
