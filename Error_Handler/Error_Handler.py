# Hoy en día esta clase se encarga de gestinar el error producido durante la ejecución del script de alertas
# Verifica si en el día de la fecha envió una alerta, en caso de no haberlo hecho, envía un mail y lo almacena en la base  

#from Base import *
from DB.Base import *

import datetime
import logging

class Error_Handler:
    
    def __init__(self):
        self.db = Base()

    def manejar_error(self, producto, error):
        logging.warning('Falló el script del producto %s : %s', producto , error)
        hoy = datetime.date.today()
        resultado = self.db.buscar_error(producto, hoy)
        if not resultado:
            logging.info('Se alertará el error')
            self.alertar_error(producto, error)
            self.db.insertar_error(producto)
        else:
            logging.info('El error ya fue advertido')


logging.basicConfig(filename='/var/log/alertasmail.log', format='%(asctime)s %(levelname)s: %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.DEBUG)




  
