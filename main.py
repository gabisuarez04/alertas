import datetime
import time
import os
import sys

import config as cfg
from Error_Handler.EH_Mail import *
from Productos import *

def str_to_module(string):
    return getattr(sys.modules["Productos"], string)

error_handler = EH_Mail()

logging.basicConfig(filename='/var/log/alertasmail.log', format='%(asctime)s %(levelname)s: %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.DEBUG)
logging.info('Comenzó a ejecutarse el script de alertas')

for producto in cfg.productos:
    sitio = cfg.productos[producto]['sitio']
    pagina_avisos = cfg.productos[producto]['pagina_avisos']
    modulo = str_to_module(producto)
    clase_producto = getattr(modulo, producto)
    instancia_producto = clase_producto(producto, sitio, pagina_avisos, error_handler)
    try:
        time.sleep(3)
        instancia_producto.actualizar_avisos()
    except Exception as error:
        logging.warning('Falló el script del producto %s : %s', producto , error)

logging.info('Terminó de ejecutarse el script de alertas')
